# pull mirror rsyncd

Simple rsyncd running as non-root in a docker container, inspired by
https://github.com/nabeken/docker-volume-container-rsync and
https://github.com/bfosberry/rsync

## Usage

Launch the container via docker, mapping the root of the rsync folder
as a docker volume:

```console
# docker run -d -p 873:8873 --name rsyncd \
    --volume /srv/fdroid-mirror.example.com/htdocs/fdroid:/fdroid-mirror \
    registry.gitlab.com/eighthave/pull-mirror-rsyncd:latest
```

To sync files from this server:

```console
$ export RSYNC_PASSWORD=dont-abuse-me-please
$ rsync -axv fdroid-mirror@fdroid-mirror.example.com::repo/  /path/to/repo/
$ rsync -axv fdroid-mirror@fdroid-mirror.example.com::archive/  /path/to/archive/
```
